"use strict" ;

// 1. Опишіть своїми словами, що таке метод об'єкту

// Це функція, яка є властивістю об'єкта

// 2. Який тип даних може мати значення властивості об'єкта?

// Значення властивості об'єкта може мати будь-який тип даних, включаючи примітивні типи та складні типи:
// Літерал об'єкта , Функції-конструктори , Метод Object.create , Класи ES6

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

// Oзначає, що змінні, які містять об'єкти, містять не сам об'єкт, а посилання на об'єкт, яке вказує на його розташування в пам'яті. 

// 1. Створіть об'єкт product з властивостями name, price та discount.
//  Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.

const product = {
 name: "Car" ,
 price: 100000,
 discount:25,

 receiveDiscount: function(){
    let newPrice = (this.price * this.discount) / 100;
    let finalPrice = this.price - newPrice ;
    return finalPrice;
 }
}


console.log("Finish price without discount:",product.receiveDiscount());

// 2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
// за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.

function greet(person) {
    return `Hello ${person.name}, I am ${person.age} years old`;
   }
   const person = {
    name: prompt('Please enter your name:'),
    age: prompt('Please enter your age:')
   };
   const result = greet(person);
   alert(result);

